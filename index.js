// alert("B249!");

// In JS, classes can be created using the "class" keyword and {}
// Naming convention for classes: Begin with Uppercase characters

/*
	Syntax:

		class <Name> {
		
		}

*/

// Here we have an empty student class
class Student {
	constructor(name, email, grades) {
		//propertyName = value
		this.name = name;
		this.email = email;
		//add this property to the constructor
		this.gradeAve = undefined;

		if(grades.length === 4){
		     if(grades.every(grade => grade >= 0 && grade <= 100)){
		         this.grades = grades;
		     } else {
		         this.grades = undefined;
		     }
		 } else {
		     this.grades = undefined;
		 }
	}

	login() {
		console.log(`${this.email} has logged in`);
		return this;
	}

	logout() {
		console.log(`${this.email} has logged out`);
		return this;
	}

	listGrades() {
		console.log(`${this.name}'s' quarterly grades are ${this.grades}`);
		return this;
	}
	computeAve() {
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		this.gradeAve = sum/4
		return this
	}
	willPass() {
		this.passed = this.computeAve() >= 85 ? true:false;
		return this
	}

	willPassWithHonors() {
		if (this.passed){
			if (this.computeAve() >= 90) {
				this.PassedWithHonors = true;
				return this
			} else {
				this.PassedWithHonors = false;
				return this
			}
		} else { 	
			this.PassedWithHonors = undefined;
			return this;
		}

		return this
		}
	}



//getter and setter
//Best practice dictates that we regulate access to such properties. We do so, via the use of "getters"(regulates retrieval) and setter (regulates manipulation).

//Method Chaining~

//Instantiation - process of creating objects from class

// To create an object from a class, use the "new" keyword. When a class has a constructor, we need to supply ALL the values needed by the constructor
let studentOne = new  Student('John', 'john@mail.com', [89, 84, 78, 88]);
console.log(studentOne);


/*
	Mini-Exercise:
		Create a new class called Person.

		This Person class should be able to instatiate a new object with the ff fields.

		name:
		age: (should be a number and must be greater than or equal to 18, otherwise, set the property to undefined)
		nationality:
		address:

		Instantiate 2 new objects from the Person class as person1 and person2

		log both objects in the console. Take a screenshot of your console and send to group chat.


*/

class Person {
	constructor(name, age, nationality, address) {
	
		this.name = name;
		if(typeof age === "number" && age >= 18) {
			this.age = age;
		} else {
			this.age = undefined;
		}

		this.nationality = nationality;
		this.address = address;
	}
}

let person1 = new  Person('John', 19, 'Filipino', 'Quezon City');
// console.log(person1);

let person2 = new  Person('Joe', 16, 'Filipino', 'Manila City');
// console.log(person2);

// ACTIVITY 1 Quiz

/*
1. Classes
2. PascalCase
3. new
4. Instantiation
5. constructor () method

*/

// ACTIVITY 1

let studentTwo = new  Student('Joe', 'joe@mail.com', ['hello', 84, 78, 88]);
console.log(studentTwo);

let studentThree = new  Student('Joe', 'joe@mail.com', [84, 78, 88]);
console.log(studentThree);

let studentFour = new  Student('Joe', 'joe@mail.com', [-10, 84, 78, 88]);
console.log(studentFour);


// ACTIVITY 2

/*
1. False
2. False
3. True
4. getter and setter
5. Object
*/

